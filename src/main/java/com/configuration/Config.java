package com.configuration;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;

@Configuration
@Import(ClassInjections.class)
@PropertySource("classpath:application.properties")
public class Config {
    @Resource
    private Environment env;


    @Bean
    public String applicationUrl(){
        return env.getProperty("url.properties");
    }
    @Bean String path(){
        return env.getProperty("path.chrome");
    }

    @Bean
    public WebDriver webDriver(String applicationUrl,String path){

        System.setProperty("webdriver.chrome.driver", path);

        WebDriver driver = new ChromeDriver();


        driver.get(applicationUrl);
        return driver;
    }
}
