package com.configuration;

import com.pages.PageElements;
import com.utilities.Utilities;
import org.openqa.selenium.WebDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ClassInjections {

    @Bean
    public Utilities utilities(WebDriver webDriver){
        return new Utilities(webDriver);

    }
    @Bean
    public PageElements pageElements(WebDriver webDriver){
        return new PageElements(webDriver);
    }
}
