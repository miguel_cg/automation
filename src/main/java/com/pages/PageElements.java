package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.concurrent.TimeUnit;

public class PageElements {

    WebDriver webDriver;

    public PageElements(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void setQuery(String sql){

        Actions actions = new Actions(webDriver);
        WebElement we = webDriver.findElement(getSQLTextArea());
        actions.moveToElement(we);
        actions.click();
        actions.doubleClick().click();
        actions.sendKeys(sql);
        actions.build().perform();
    }

    public void sendQuery()  {
        webDriver.findElement(getSendButton()).click();
        webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
    }

    public By getRecordsCreateTable(){
        return By.xpath("//*[@id=\"yourDB\"]/table/tbody/tr[10]/td[2]");
    }
    public  By getRestoreDb(){
        return By.id("restoreDBBtn");
    }
    public  By getSqlHeader(){
        return By.xpath("//h3[contains(.,'SQL Statement:')]");
    }
    public  By getIframe(){
        return By.xpath(".//*[@id='google_ads_iframe_/16833175/TryitLeaderboard_0']");
    }

    public  By getDbInfo(){
        return By.id("dbInfo");
    }
    public  By getWebBottom(){
        return By.xpath("html/body/div[2]/div/div[1]/h3");
    }

    public  By getResults() {
        return By.xpath("//h3[contains(.,'Result:')]");
    }

    public By getNewTableElement(){
        return By.xpath("//*[@id=\"yourDB\"]/table/tbody/tr[10]/td[1]");
    }
    private By getSQLTextArea(){
        return By.xpath("//PRE[@class=' CodeMirror-line ']/self::PRE");
    }

    private By getSendButton(){
        return By.cssSelector("button.w3-green.w3-btn");
    }
    public By getResultsTextArea(){
        return By.id("divResultSQL");
    }
    public By getQueryResults(){ return By.xpath("//*[@id=\"divResultSQL\"]/div/table/tbody/tr[2]");}
}
