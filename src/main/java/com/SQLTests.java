package com;

import com.hooks.QueryResultsHook;
import com.hooks.SQLHooks;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import java.util.concurrent.TimeUnit;


public class SQLTests extends BaseTest {

    @BeforeMethod
    public void before()  {
        driver.findElement(pageElements.getRestoreDb()).click();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.switchTo().alert().accept();

    }
    @Test
    public void createTable() throws Exception {
        pageElements.setQuery(SQLHooks.createTable());
        pageElements.sendQuery();
        utilities.checkElementExists(pageElements.getNewTableElement());
        utilities.assertValues(pageElements.getNewTableElement(),"easyTable");
        utilities.assertValues(pageElements.getRecordsCreateTable(),"0");

    }
    @Test
    public void checkDefaultText(){
        utilities.assertValues(pageElements.getResultsTextArea(), QueryResultsHook.getDefaultText());
    }
    @Test
    public void spanishCustomer() throws InterruptedException {
        pageElements.setQuery(SQLHooks.getCustomersFromSpain());
        pageElements.sendQuery();
        utilities.assertValues(pageElements.getQueryResults(),QueryResultsHook.getSpanishResults());
    }
    @Test
    public void checkNoteFromEmployee(){
        pageElements.setQuery(SQLHooks.getaOldEmployee());
        pageElements.sendQuery();
        utilities.assertValues(pageElements.getQueryResults(),QueryResultsHook.getOldEmployeeNote());
    }

    @Test
    public void checkLongestOrder(){
        pageElements.setQuery(SQLHooks.getOrders());
        pageElements.sendQuery();
        utilities.assertValues(pageElements.getQueryResults(),QueryResultsHook.getLargestOrder());

    }
    @Test
    public void checkOrderByEmployee(){
        pageElements.setQuery(SQLHooks.ordersByEmployee());
        pageElements.sendQuery();
        utilities.assertValues(pageElements.getQueryResults(),QueryResultsHook.getAllOrderEmployee());
    }
}
