package com;

import org.testng.annotations.Test;

public class WebTests extends BaseTest{

    @Test
    public void checkWebElements() throws Exception {
        utilities.checkElementExists(pageElements.getIframe());
        utilities.checkElementExists(pageElements.getDbInfo());
        utilities.checkElementExists(pageElements.getSqlHeader());
        utilities.checkElementExists(pageElements.getResults());
        utilities.checkElementExists(pageElements.getWebBottom());

    }
}
