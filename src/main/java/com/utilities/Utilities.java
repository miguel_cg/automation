package com.utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class Utilities {
    private WebDriver driver;


    public Utilities(WebDriver driver) {
        this.driver = driver;
    }

    public void checkElementExists(By element) throws Exception {
        WebElement we = driver.findElement(element);
        if(!we.isDisplayed()){
            throw new Exception("The element "+element.toString()+" is not visible");
        }
    }
    public void assertValues(By element,String expected){
        String actual=driver.findElement(element).getText();
        Assert.assertEquals(actual,expected);
    }
}
