package com;

import com.configuration.Config;
import com.pages.PageElements;
import com.utilities.Utilities;
import org.openqa.selenium.WebDriver;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterSuite;

import javax.annotation.Resource;


@ContextConfiguration(classes = {Config.class})
public class BaseTest  extends AbstractTestNGSpringContextTests {
@Resource
public WebDriver driver;
@Resource
 public Utilities utilities;
@Resource
public PageElements pageElements;

@AfterSuite
 public void tearDown(){
 driver.close();
}

}
