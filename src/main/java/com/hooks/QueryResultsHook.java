package com.hooks;

public class QueryResultsHook {



    public static String getDefaultText(){
        return "Click \"Run SQL\" to execute the SQL statement above.\n" +
                "W3Schools has created an SQL database in your browser.\n" +
                "The menu to the right displays the database, and will reflect any changes.\n" +
                "Feel free to experiment with any SQL statement.\n" +
                "You can restore the database at any time.";
    }
    public static String getSpanishResults(){
        return "3";
    }
    public static String getOldEmployeeNote(){
        return "An old chum.";
    }
    public static String getLargestOrder(){
        return "Longlife Tofu 100";
    }
    public static String getAllOrderEmployee(){
        return "4";
    }
}
