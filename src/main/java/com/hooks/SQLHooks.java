package com.hooks;

public class SQLHooks {

    public static String createTable(){
        return "create table easyTable(name varchar)";
    }
    public static String getCustomersFromSpain(){
        return "select count(*) as total from customers where country = 'Spain'";
    }

    public static String getaOldEmployee(){
        return "select notes from employees where birthdate between '1925-01-01' and '1930-12-31'";
    }

   public static String getOrders(){
        return "SELECT pro.productname,ord.quantity FROM Products pro,orderdetails ord where pro.productid=ord.productid order by ord.quantity desc";

   }
   public static String ordersByEmployee(){
       return "SELECT distinct(employeeID) FROM orders where orderdate='1997-01-30'";
   }
}
